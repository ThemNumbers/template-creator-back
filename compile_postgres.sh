#!/bin/sh

javac -classpath /Library/glassfish5/glassfish/modules/jakarta.servlet-api.jar:./second/WEB-INF/lib/postgresql-42.2.8.jar:. ./second/WEB-INF/classes/controller/WebController.java

cd second; zip -r -X ../second.war *; cd ..
