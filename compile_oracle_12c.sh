#!/bin/sh

javac -classpath /Library/glassfish5/glassfish/modules/jakarta.servlet-api.jar:./second/WEB-INF/lib/ojdbc6.jar:. ./second/WEB-INF/classes/controller/WebController.java

cd second; zip -r -X ../second.war *; cd ..

cp ./second.war /Library/glassfish5/glassfish/domains/domain1/autodeploy/
