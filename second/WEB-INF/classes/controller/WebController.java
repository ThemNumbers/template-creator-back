package controller;

import com.google.gson.Gson;
import models.Basic;
import oracle.jdbc.driver.OracleDriver;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.*;


public class WebController extends HttpServlet
{
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  {
      response.setContentType("text/html;charset=cp1251");

      String login = request.getParameter("login");
      String password = request.getParameter("password");

      String responseString = "";
    
    try {
        DriverManager.registerDriver(new OracleDriver());

        //Connection conn = DriverManager.getConnection ("jdbc:oracle:thin:@82.179.14.185:1521:nmics", "stud06", "stud06");
        Connection conn = DriverManager.getConnection ("jdbc:oracle:thin:@81.1.193.101:1521:orcl", "dev", "Dt4yjcnm");

        //Statement stmt = conn.createStatement(); //обычный запрос
        //PreparedStatement pstmt = conn.prepareStatement ("insert into EMP (EMPNO, ENAME) values (?, ?)"); //с параметрами запрос
        //CallableStatement clstmt = conn.prepareCall("stud04.LAB3"); //вызов функции
        //PreparedStatement stmt = conn.prepareStatement("select message from error_log where id_log = ?");
        //stmt.setInt(1,1);
        PreparedStatement stmt = conn.prepareStatement (
            "select count(*) " +
            "from dev.accounts a " +
            "where a.email = ? " +
            "and a.password = ?"
        );
        stmt.setString(1,login);
        stmt.setString(2,password);

        ResultSet rset = stmt.executeQuery();
        //ResultSet rset = stmt.executeQuery ("select last_name, first_name from s_emp");
        //ResultSet rset = stmt.executeQuery ("select * from user");

        /*while (rset.next())
        responseString += rset.getString(1);//+" "+rset.getString(2);

         */
        rset.next();
        int result = rset.getInt(1);

        Gson gson = new Gson();
        if (result>0) {
            responseString = gson.toJson(new Basic(1, "Успех"));
            //response.setStatus(200);
        } else {
            responseString = gson.toJson(new Basic(-2, "Ошибка"));
            //response.setStatus(400);
        }

        rset.close();
        stmt.close();
    } catch (SQLException ex) {
        responseString = "SQLException: "+ex.getMessage();
    }
    
    PrintWriter printWriter = null;
    try {printWriter = response.getWriter();}
    catch (Exception ex) {}

    try {printWriter.println(responseString);}    
    catch (Exception ex) {printWriter.println("Error: "+ex.getMessage());}
  }
}
